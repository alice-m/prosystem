SOURCEDIR := $(abspath $(patsubst %/,%,$(dir $(abspath $(lastword \
	$(MAKEFILE_LIST))))))

NAME := prosystem
JGNAME := $(NAME)-jg

SRCDIR := $(SOURCEDIR)/src

# Only relative include paths are used in prosystem
INCLUDES_JG = -I$(SRCDIR)

LINKER = $(CC)

LIBS =
LIBS_STATIC =

LIBS_REQUIRES :=

DOCS := LICENSE README

# Object dirs
MKDIRS :=

override INSTALL_DATA := 1
override INSTALL_EXAMPLE := 0
override INSTALL_SHARED := 0

include $(SOURCEDIR)/version.h
include $(SOURCEDIR)/mk/jg.mk

EXT := c
FLAGS := -std=c99 $(WARNINGS_DEF_C)

CSRCS := md5.c \
	Bios.c \
	Cartridge.c \
	Database.c \
	Maria.c \
	ExpansionModule.c \
	Memory.c \
	Palette.c \
	Pokey.c \
	ProSystem.c \
	Region.c \
	Riot.c \
	Sally.c \
	Sound.c \
	Tia.c

JGSRCS := jg.c

# Assets
DATA := ProSystem.dat

DATA_TARGET := $(DATA:%=$(NAME)/%)

# List of object files
OBJS := $(patsubst %,$(OBJDIR)/%,$(CSRCS:.c=.o))
OBJS_JG := $(patsubst %,$(OBJDIR)/%,$(JGSRCS:.c=.o))

# Core commands
BUILD_JG = $(call COMPILE_C, $(FLAGS) $(INCLUDES_JG) $(CFLAGS_JG))
BUILD_MAIN = $(call COMPILE_C, $(FLAGS))

.PHONY: $(PHONY)

all: $(TARGET)

# Rules
$(OBJDIR)/%.o: $(SRCDIR)/%.$(EXT) $(PREREQ)
	$(call COMPILE_INFO,$(BUILD_MAIN))
	@$(BUILD_MAIN)

# Data rules
$(DATA_TARGET): $(DATA:%=$(SOURCEDIR)/%)
	@mkdir -p $(NAME)
	@cp $(DATA:%=$(SOURCEDIR)/%) $(NAME)/

install-data: all
	@mkdir -p $(DESTDIR)$(DATADIR)/jollygood/$(NAME)
	cp $(DATA_OUT)/ProSystem.dat $(DESTDIR)$(DATADIR)/jollygood/$(NAME)/

include $(SOURCEDIR)/mk/rules.mk
