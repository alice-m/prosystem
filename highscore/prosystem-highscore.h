#pragma once

#include <highscore/libhighscore.h>

G_BEGIN_DECLS

#define PROSYSTEM_TYPE_CORE (prosystem_core_get_type())

G_DECLARE_FINAL_TYPE (ProSystemCore, prosystem_core, PROSYSTEM, CORE, HsCore)

G_MODULE_EXPORT GType hs_get_core_type (void);

G_END_DECLS