#include "prosystem-highscore.h"

#include <math.h>
#include <stdbool.h>
#include <stdint.h>

#include "../src/Database.h"
#include "../src/Maria.h"
#include "../src/Palette.h"
#include "../src/ProSystem.h"
#include "../src/Sound.h"

#define SAMPLE_RATE 48000

struct _ProSystemCore
{
  HsCore parent_instance;

  HsSoftwareContext *context;

  uint8_t *audio_buffer_u8;
  int16_t *audio_buffer_i16;

  uint8_t input_state[17];
};

static void prosystem_atari_7800_core_init (HsAtari7800CoreInterface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (ProSystemCore, prosystem_core, HS_TYPE_CORE,
                               G_IMPLEMENT_INTERFACE (HS_TYPE_ATARI_7800_CORE, prosystem_atari_7800_core_init));

static gboolean
prosystem_core_load_rom (HsCore      *core,
                         const char **rom_paths,
                         int          n_rom_paths,
                         const char  *save_path,
                         GError     **error)
{
  ProSystemCore *self = PROSYSTEM_CORE (core);
  g_autofree char *data = NULL;
  gsize length;

  g_assert (n_rom_paths == 1);

  if (!g_file_get_contents (rom_paths[0], &data, &length, error))
    return FALSE;

  if (!cartridge_Load ((unsigned char *) data, length)) {
    g_set_error (error, HS_CORE_ERROR, HS_CORE_ERROR_INTERNAL, "Failed to load ROM");
    return FALSE;
  }

  g_autofree char *db_path = g_build_filename (CORE_DIR, "Prosystem.dat", NULL);

  database_filename = db_path;
  database_enabled = true;

  database_Load (cart_digest);

  sound_SetSampleRate (SAMPLE_RATE);
  prosystem_Reset();

  if (cartridge_controller[0] & CARTRIDGE_CONTROLLER_LIGHTGUN)
    self->input_state[3] = 1;

  self->input_state[15] = cartridge_left_switch;
  self->input_state[16] = cartridge_right_switch;

  self->context = hs_core_create_software_context (core, 320, 292, HS_PIXEL_FORMAT_R8G8B8);

  return TRUE;
}

static void
prosystem_core_poll_input (HsCore *core, HsInputState *input_state)
{
  ProSystemCore *self = PROSYSTEM_CORE (core);

  if (cartridge_controller[0] & CARTRIDGE_CONTROLLER_LIGHTGUN) {
    int x_offset = -10;
    int y_offset = (cartridge_region == REGION_NTSC ? -3 : 8);

    self->input_state[3] = input_state->atari_7800.lightgun_fire ^ 1;

    int x = (int) round (input_state->atari_7800.lightgun_x * (maria_displayArea.right - maria_displayArea.left + 1));
    int y = (int) round (input_state->atari_7800.lightgun_y * (maria_visibleArea.bottom - maria_visibleArea.top + 1));

    lightgun_cycle = x + HBLANK_CYCLES + LG_CYCLES_INDENT + x_offset;
    lightgun_scanline = y + maria_displayArea.top + y_offset;

    if (lightgun_cycle > CYCLES_PER_SCANLINE) {
      lightgun_scanline++;
      lightgun_cycle -= CYCLES_PER_SCANLINE;
    }
  } else {
    self->input_state[0] = input_state->atari_7800.joystick[0] & 1 << HS_ATARI_7800_JOYSTICK_BUTTON_RIGHT;
    self->input_state[1] = input_state->atari_7800.joystick[0] & 1 << HS_ATARI_7800_JOYSTICK_BUTTON_LEFT;
    self->input_state[2] = input_state->atari_7800.joystick[0] & 1 << HS_ATARI_7800_JOYSTICK_BUTTON_DOWN;
    self->input_state[3] = input_state->atari_7800.joystick[0] & 1 << HS_ATARI_7800_JOYSTICK_BUTTON_UP;
    self->input_state[4] = input_state->atari_7800.joystick[0] & 1 << HS_ATARI_7800_JOYSTICK_BUTTON_ONE;
    self->input_state[5] = input_state->atari_7800.joystick[0] & 1 << HS_ATARI_7800_JOYSTICK_BUTTON_TWO;
  }

  self->input_state[6]  = input_state->atari_7800.joystick[1] & 1 << HS_ATARI_7800_JOYSTICK_BUTTON_RIGHT;
  self->input_state[7]  = input_state->atari_7800.joystick[1] & 1 << HS_ATARI_7800_JOYSTICK_BUTTON_LEFT;
  self->input_state[8]  = input_state->atari_7800.joystick[1] & 1 << HS_ATARI_7800_JOYSTICK_BUTTON_DOWN;
  self->input_state[9]  = input_state->atari_7800.joystick[1] & 1 << HS_ATARI_7800_JOYSTICK_BUTTON_UP;
  self->input_state[10] = input_state->atari_7800.joystick[1] & 1 << HS_ATARI_7800_JOYSTICK_BUTTON_ONE;
  self->input_state[11] = input_state->atari_7800.joystick[1] & 1 << HS_ATARI_7800_JOYSTICK_BUTTON_TWO;

  self->input_state[13] = input_state->atari_7800.select_button;
  self->input_state[14] = input_state->atari_7800.pause_button;
  self->input_state[15] = input_state->atari_7800.difficulty[0];
  self->input_state[16] = input_state->atari_7800.difficulty[1];
}

static void
prosystem_core_run_frame (HsCore *core)
{
  ProSystemCore *self = PROSYSTEM_CORE (core);

  prosystem_ExecuteFrame (self->input_state);

  self->input_state[12] = FALSE;

  hs_software_context_set_area (self->context,
                                &HS_RECTANGLE_INIT (maria_displayArea.left,
                                                    maria_visibleArea.top - maria_displayArea.top,
                                                    maria_displayArea.right  - maria_displayArea.left + 1,
                                                    maria_visibleArea.bottom - maria_visibleArea.top  + 1));

  uint8_t *buffer = hs_software_context_get_framebuffer (self->context);

  for (int x = maria_displayArea.left; x <= maria_displayArea.right; x++) {
    for (int y = maria_visibleArea.top - maria_displayArea.top; y <= maria_visibleArea.bottom - maria_displayArea.top; y++) {
      uint8_t color = maria_surface[y * 320 + x];

      buffer[(y * 320 + x) * 3 + 0] = palette_data[color * 3 + 0];
      buffer[(y * 320 + x) * 3 + 1] = palette_data[color * 3 + 1];
      buffer[(y * 320 + x) * 3 + 2] = palette_data[color * 3 + 2];
    }
  }

  int size = sound_Store (self->audio_buffer_u8);

  for (int i = 0; i < size; i++)
    self->audio_buffer_i16[i] = self->audio_buffer_u8[i] << 8;

  hs_core_play_samples (core, self->audio_buffer_i16, size);
}

static void
prosystem_core_reset (HsCore *core, gboolean hard)
{
  ProSystemCore *self = PROSYSTEM_CORE (core);

  if (hard)
    prosystem_Reset ();
  else
    self->input_state[12] = TRUE;
}

static void
prosystem_core_stop (HsCore *core)
{
  ProSystemCore *self = PROSYSTEM_CORE (core);

  g_clear_object (&self->context);
}

static void
prosystem_core_pause (HsCore *core)
{
  prosystem_Pause (true);
}

static void
prosystem_core_resume (HsCore *core)
{
  prosystem_Pause (false);
}

static gboolean
prosystem_core_reload_save (HsCore      *core,
                            const char  *save_path,
                            GError     **error)
{
  return TRUE;
}

static void
prosystem_core_load_state (HsCore          *core,
                           const char      *path,
                           HsStateCallback  callback)
{
  if (!prosystem_Load (path)) {
    GError *error = NULL;
    g_set_error (&error, HS_CORE_ERROR, HS_CORE_ERROR_INTERNAL, "Failed to load state");
    callback (core, &error);
    return;
  }

  callback (core, NULL);
}

static void
prosystem_core_save_state (HsCore          *core,
                           const char      *path,
                           HsStateCallback  callback)
{
  if (!prosystem_Save (path)) {
    GError *error = NULL;
    g_set_error (&error, HS_CORE_ERROR, HS_CORE_ERROR_INTERNAL, "Failed to save state");
    callback (core, &error);
    return;
  }

  callback (core, NULL);
}

static double
prosystem_core_get_frame_rate (HsCore *core)
{
  return cartridge_region == REGION_NTSC ? 60 : 50;
}

static double
prosystem_core_get_aspect_ratio (HsCore *core)
{
  int width = maria_visibleArea.right - maria_visibleArea.left + 1;
  int height = maria_visibleArea.bottom - maria_visibleArea.top + 1;
  double multiplier;

  // Source: https://sites.google.com/site/atari7800wiki/7800-compared-to-the-nes
  if (cartridge_region == REGION_NTSC)
    multiplier = 6.0 / 7.0;
  else
    multiplier = 1.040;

  return (double) width / (double) height * multiplier;
}

static double
prosystem_core_get_sample_rate (HsCore *core)
{
  return SAMPLE_RATE;
}

static int
prosystem_core_get_channels (HsCore *core)
{
  return 1;
}

static HsRegion
prosystem_core_get_region (HsCore *core)
{
  if (cartridge_region == REGION_NTSC)
    return HS_REGION_NTSC;
  else
    return HS_REGION_PAL;
}

static void
prosystem_core_finalize (GObject *object)
{
  ProSystemCore *self = PROSYSTEM_CORE (object);

  g_free (self->audio_buffer_u8);
  g_free (self->audio_buffer_i16);

  G_OBJECT_CLASS (prosystem_core_parent_class)->finalize (object);
}

static void
prosystem_core_class_init (ProSystemCoreClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  HsCoreClass *core_class = HS_CORE_CLASS (klass);

  object_class->finalize = prosystem_core_finalize;

  core_class->load_rom = prosystem_core_load_rom;
  core_class->poll_input = prosystem_core_poll_input;
  core_class->run_frame = prosystem_core_run_frame;
  core_class->reset = prosystem_core_reset;
  core_class->stop = prosystem_core_stop;
  core_class->pause = prosystem_core_pause;
  core_class->resume = prosystem_core_resume;

  core_class->reload_save = prosystem_core_reload_save;

  core_class->load_state = prosystem_core_load_state;
  core_class->save_state = prosystem_core_save_state;

  core_class->get_frame_rate = prosystem_core_get_frame_rate;
  core_class->get_aspect_ratio = prosystem_core_get_aspect_ratio;

  core_class->get_sample_rate = prosystem_core_get_sample_rate;
  core_class->get_channels = prosystem_core_get_channels;

  core_class->get_region = prosystem_core_get_region;
}

static void
prosystem_core_init (ProSystemCore *self)
{
  self->audio_buffer_u8 = g_new0 (uint8_t, 8192);
  self->audio_buffer_i16 = g_new0 (int16_t, 8192);
}

static HsAtari7800Controller
prosystem_atari_7800_get_controller (HsAtari7800Core *core, guint player)
{
  if (cartridge_controller[player] & HS_ATARI_7800_CONTROLLER_LIGHTGUN)
    return HS_ATARI_7800_CONTROLLER_LIGHTGUN;

  if (cartridge_controller[player] & HS_ATARI_7800_CONTROLLER_JOYSTICK)
    return HS_ATARI_7800_CONTROLLER_JOYSTICK;

  return HS_ATARI_7800_CONTROLLER_NONE;
}

static HsAtari7800Difficulty
prosystem_atari_7800_get_default_difficulty (HsAtari7800Core *core, guint player)
{
  return player == 0 ? cartridge_left_switch : cartridge_right_switch;
}

static void
prosystem_atari_7800_core_init (HsAtari7800CoreInterface *iface)
{
  iface->get_controller = prosystem_atari_7800_get_controller;
  iface->get_default_difficulty = prosystem_atari_7800_get_default_difficulty;
}

GType
hs_get_core_type (void)
{
  return PROSYSTEM_TYPE_CORE;
}
